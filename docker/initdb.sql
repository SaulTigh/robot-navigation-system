CREATE TABLE if not exists landmarks
(
    x    INTEGER NOT NULL,
    y    INTEGER NOT NULL,
    name VARCHAR NOT NULL,
    UNIQUE (x, y),
    UNIQUE (name)
);

CREATE TABLE if not exists routes
(
    id   SERIAL NOT NULL,
    path JSON   NOT NULL,
    PRIMARY KEY (id)
);
