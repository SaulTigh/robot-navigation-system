from aiohttp import web
from roboboba.app import app


def main():
    web.run_app(app)


