import argparse
import dataclasses as d
from functools import partial
from typing import Iterable
from typing import List
from typing import Optional
from typing import Tuple

import requests
from toolz import compose_left

ServerPoint = Tuple[int, int, Optional[str]]

API_URL = 'http://localhost:8080/routes'
_log = partial(print, 'ROBOT SAY:')


@d.dataclass(frozen=True)
class RouteCoords:
    x: int
    y: int
    landmark: Optional[str]

    @classmethod
    def from_server_point(cls, point: ServerPoint):
        x, y, landmark = point
        return cls(x, y, landmark)

    def point_str(self) -> str:
        return f'point({self.x}, {self.y})'


def preprocess_route(route: list) -> Iterable[RouteCoords]:
    return map(RouteCoords.from_server_point, route)


def request_route(id_: int) -> List[ServerPoint]:
    return requests.get(API_URL, params={'id': id_}).json()


def navigate_route(route: Iterable[RouteCoords]) -> None:
    for coord in route:
        _log('Navigating to', coord.point_str())
        if coord.landmark is not None:
            _log(f'Reached "{coord.landmark}"')


parser = argparse.ArgumentParser(
    prog='robot',
    description='This robot likes to walk the city',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument('route_id', help='Request route by it\'s ID', type=int)


def main():
    ns = parser.parse_args()
    compose_left(request_route, preprocess_route, navigate_route)(ns.route_id)
