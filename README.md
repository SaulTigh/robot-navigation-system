ROBOBOBA
========

A robot navigation system.

#### Installation

Prerequisites: docker-compose, pyenv.

* `pyenv virtualenv 3.7 roboboba_env`
* `pyenv activate roboboba_env`
* `pip install poetry`
* `poetry develop`
* `docker-compose build`

#### Running

* `docker-compose up -d` starts a postgres database in background
* `poetry run server` runs the server itself
* `poetry run robot` run robot, accepts route_id as a positional argument

#### Preparations

Test data is provided in `data` subdirectory. Run db and server before you can load test data like
so:

* `http POST http://localhost:8080/landmarks < data/test_landmarks.json` load test landmarks
* `http POST localhost:8080/routes < data/test_route_1.json` cjload fist test route, it gives 
route id in return
* `http POST localhost:8080/routes < data/test_route_2.json` load second test route, it 
gives all available landmarks on the server

*NOTE:* HTTPie is used as example, you can use curl if you like. Obviously it should be installed
in your system.


### Example of running with preparations

* `docker-compoes up -d`
* `poetry run server`
* `http POST http://localhost:8080/landmarks < data/test_landmarks.json`
* `http POST localhost:8080/routes < data/test_route_1.json`
* `poetry run robot 1`


### Testing

Tests are running using pytest like so:

* `poetry run pytest tests`
