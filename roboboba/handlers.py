import logging

from aiohttp import web

from roboboba import bl
from roboboba import utils as u

log = logging.getLogger(__name__)


async def load_routes_json(request: web.Request) -> web.Response:
    route = await request.json()
    log.info('Loading route "%s" into database', route)
    return web.json_response({'id': await bl.load_routes_into_db(request.app, route)})


async def load_landmarks_json(request: web.Request) -> web.Response:
    landmarks = await request.json()
    log.info('Loading landmarks "%s" into database', landmarks)
    return web.json_response({
        'available_landmarks': await bl.load_landmarks_into_db(request.app, landmarks),
    })


async def retrieve_routes_json(request: web.Request) -> web.Response:
    id_ = request.query.get('id')
    log.info('Robot requested route id: %d', id_)
    db_routes = await bl.retrieve_routes_from_db(request.app, id_)
    coordinates = u.build_coordinates(db_routes)
    return web.json_response(await bl.build_robot_instructions(request.app, coordinates))
