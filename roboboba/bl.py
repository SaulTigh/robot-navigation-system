import sqlalchemy as sa

from roboboba import db
from roboboba import utils as u


async def load_routes_into_db(app, path) -> int:
    query = db.routes.insert().values({'path': path}).returning(db.routes.c.id)
    async with app['db'].acquire() as connection:
        return await connection.scalar(query)


async def load_landmarks_into_db(app, landmarks) -> list:
    query = db.landmarks.insert().values(landmarks)
    async with app['db'].acquire() as connection:
        await connection.execute(query)
        return [dict(lm) async for lm in await connection.execute(db.landmarks.select())]


async def build_robot_instructions(app, coordinates: list) -> list:
    query = (
        sa.select([
            db.landmarks.c.x, db.landmarks.c.y,
            db.landmarks.c.name,
        ])
        .select_from(db.landmarks)
        .where(sa.or_(*[
            sa.and_(
                db.landmarks.c.x == x,
                db.landmarks.c.y == y,
            ) for (x, y) in coordinates
        ]))
    )

    async with app['db'].acquire() as connection:
        found_lms = [i.as_tuple() for i in await (await connection.execute(query)).fetchall()]

    def create_robot_coordinate(coords):
        for lm in found_lms:
            if u.are_identical(coords, lm[:2]):
                return lm
        x, y = coords
        return x, y, None

    return list(map(create_robot_coordinate, coordinates))


async def retrieve_routes_from_db(app, id_):
    query = sa.select([db.routes.c.path]).where(db.routes.c.id == id_)
    async with app['db'].acquire() as connection:
        return await connection.scalar(query)
