import aiopg.sa
from aiohttp import web
from roboboba import handlers as h

app = web.Application()

config = {
    'postgres': {
        'database': 'roboboba',
        'user': 'roboboba_pg',
        'password': 'secret',
        'host': 'localhost',
        'port': '15432',
    },
}

async def init_conf(app):
    app['config'] = config


async def init_pg(app):
    conf = app['config']['postgres']
    app['db'] = await aiopg.sa.create_engine(
        database=conf['database'],
        user=conf['user'],
        password=conf['password'],
        host=conf['host'],
        port=conf['port'],
    )


async def dispose_pg(app):
    app['db'].close()
    await app['db'].wait_closed()

# routes
app.router.add_get('/routes', h.retrieve_routes_json)
app.router.add_post('/routes', h.load_routes_json)
app.router.add_post('/landmarks', h.load_landmarks_json)

app.on_startup.append(init_conf)
app.on_startup.append(init_pg)
app.on_cleanup.append(dispose_pg)
