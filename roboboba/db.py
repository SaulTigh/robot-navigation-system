import sqlalchemy as sa

meta = sa.MetaData()

landmarks = sa.Table(
    'landmarks', meta,
    sa.Column('x', sa.Integer, nullable=False),
    sa.Column('y', sa.Integer, nullable=False),
    sa.Column('name', sa.String, nullable=False),
    sa.UniqueConstraint('x', 'y'),
    sa.UniqueConstraint('name'),
)

routes = sa.Table(
    'routes', meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('path', sa.JSON, nullable=False),
)
