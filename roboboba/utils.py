import math
import operator
from functools import partial

from toolz import first
from toolz import last
from toolz import diff

DIRECTIONS = ('N', 'E', 'S', 'W')


def calculate_direction(from_point, to_point):
    """Calculates arc tangents and finds in which direction
    we moved in second point.

    limitations: only tracks for 90 degrees turns (no support for NE, SW etc).
    """
    degrees = math.atan2(
        first(to_point) - first(from_point),
        last(to_point) - last(from_point),
    ) / math.pi * 180

    return DIRECTIONS[round((degrees if degrees >= 0 else degrees + 360) / 90)]


def build_coordinates(routes: list) -> list:
    """Expand given instructions to a sequence of coordinates."""
    def _process_go(cmd):
        """Process GO instruction.

        This function guesses direction if not provided and creates series of
        coordinates with cardinality equal to number of provided blocks.

        This means, if we are moving 5 blocks to North from point (0.0),
        this function will create 5 coordinates like this:

        >>> [(0, 1), (0, 2), (0, 3), (0, 4), (0, 5)]
        """
        direction, blocks = cmd
        if direction is None:
            new_cmd = (calculate_direction(*coords_path[-2:]), blocks)
            return _process_go(new_cmd)

        def _op_x(op, x, y, i):
            return op(x, i), y

        def _op_y(op, x, y, i):
            return x, op(y, i)

        def _go(coords, incr_func):
            x, y = coords
            return [incr_func(x, y, i) for i in range(1, blocks + 1)]

        if direction == 'N':
            func = partial(_op_y, operator.add)
        elif direction == 'S':
            func = partial(_op_y, operator.sub)
        elif direction == 'E':
            func = partial(_op_x, operator.add)
        elif direction == 'W':
            func = partial(_op_x, operator.sub)

        return _go(last(coords_path), func)

    def _process_turn(cmd):
        """Process TURN instruction.

        This function guesses previous direction and applies a new GO instruction
        with new direction and a distance equal 1 block.
        """
        turn, = cmd
        direction = calculate_direction(*coords_path[-2:])
        available_directions = tuple({'R': DIRECTIONS, 'L': reversed(DIRECTIONS)}[turn])
        ix = available_directions.index(direction)
        new_direction = available_directions[0 if ix == len(available_directions) - 1 else ix + 1]
        return _process_go((new_direction, 1))

    operators_processors = {
        'GO': _process_go,
        'TURN': _process_turn,
    }

    coords_path = [last(first(routes))]  # start instruction handlings
    for cmd, *body in routes[1:]:
        coords_path.extend(operators_processors[cmd](body))
    return coords_path


def are_identical(*seqs):
    """Checks if all sequences are identical without type coercion."""
    return not any(diff(*seqs, default=object()))
