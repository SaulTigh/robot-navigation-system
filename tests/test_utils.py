import pytest

from roboboba import utils as u


@pytest.mark.parametrize(
    'start_point,dest_point,expected',
    [
        ((0, 0), (1, 0), 'E'),
        ((0, 0), (0, 1), 'N'),
        ((1, 1), (0, 1), 'W'),
        ((1, 1), (1, 0), 'S'),
    ]
)
def test_calculate_direction(start_point: tuple, dest_point: tuple, expected: str):
    assert u.calculate_direction(start_point, dest_point) == expected


@pytest.mark.parametrize(
    'first_iter,second_iter',
    [
        ([1, 2, 3], (1, 2, 3)),
        ((1, 2, 3), [1, 2, 3]),
        (('1', '2', '3'), '123'),
        pytest.param(('1', '2', 3), '123', marks=pytest.mark.xfail(reason='are not equal')),
    ]
)
def test_are_identical(first_iter, second_iter):
    assert u.are_identical(first_iter, second_iter)


@pytest.mark.parametrize(
    'route,expected',
    [
        (
                [('START', (0, 0)), ('GO', 'N', 5), ('TURN', 'R'), ('GO', None, 7)],
                [
                    (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (1, 5), (2, 5), (3, 5),
                    (4, 5), (5, 5), (6, 5), (7, 5), (8, 5),
                ],
        ),
        (
                [('START', (0, 0)), ('GO', 'N', 5), ('TURN', 'R'), ('TURN', 'R'), ('GO', None, 2)],
                [
                    (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (1, 5), (1, 4),
                    (1, 3), (1, 2),
                ],
        ),
    ]
)
def test_build_coordinates(route: list, expected: list):
    coordinates = u.build_coordinates(route)
    assert u.are_identical(coordinates, expected)
